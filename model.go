package main

import (
	"context"
	"errors"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type user struct {
	Username  string `bson:"Username" json:"Username"`
	FirstName string `bson:"FirstName" json:"FirstName"`
	LastName  string `bson:"LastName" json:"LastName"`
	Password  string `bson:"Password" json:"Password"`
}

func (u *user) getUser(db *mongo.Database) error {
	return errors.New("not implemented")
}

func (u *user) updateUser(db *mongo.Database) error {
	return errors.New("not implemented")
}

func (u *user) deleteUser(db *mongo.Database) error {
	if len(u.Username) > 0 {
		filter := bson.D{{"_id", u.Username}}
		_, err := db.Collection("users").DeleteOne(context.TODO(), filter)
		if err == nil {
			log.Printf("deleted user id: %s", u.Username)
			return nil
		} else {
			log.Printf("deleted user id: %s failed: %s", u.Username, err)
		}
	}
	return errors.New("deleting user failed")
}

func (u *user) upsertUser(db *mongo.Database) error {
	if len(u.LastName) > 0 && len(u.FirstName) > 0 && len(u.LastName) > 0 && len(u.Password) > 0 {
		filter := bson.D{{"_id", u.Username}}
		update := bson.D{{"$set", bson.D{{"_id", u.Username}, {"Username", u.Username}, {"FirstName", u.FirstName}, {"LastName", u.LastName}, {"Password", u.Password}, {"UpdatedAt", time.Now()}}}}
		opts := options.Update().SetUpsert(true)
		result, err := db.Collection("users").UpdateOne(context.TODO(), filter, update, opts)
		if err == nil {
			if result.MatchedCount != 0 {
				log.Print("matched and replaced an existing document")
				return nil
			}
			if result.UpsertedCount != 0 {
				log.Printf("inserted a new document with ID %v", result.UpsertedID)
				return nil
			}
		}
		return err
	}
	return errors.New("user is not valid")
}

func getUsers(db *mongo.Database) ([]user, error) {
	var users []user
	cursor, err := db.Collection("users").Find(context.TODO(), bson.M{})
	if err != nil {
		return []user{}, err
	}
	defer func(cursor *mongo.Cursor, ctx context.Context) {
		err := cursor.Close(ctx)
		if err != nil {
			log.Printf("closing cursor failed: %s", err)
		}
	}(cursor, context.TODO())
	for cursor.Next(context.TODO()) {
		var currentUser user
		if err = cursor.Decode(&currentUser); err != nil {
			log.Printf("decoding user failed: err")
		}
		users = append(users, currentUser)
	}
	return users, nil
}
