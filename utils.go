package main

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"io/ioutil"
	"log"
	"os"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func readSSLCertificates() []tls.Certificate {

	cert, err := tls.LoadX509KeyPair(os.Getenv("SSL_PUBLIC"), os.Getenv("SSL_PRIVATE"))
	if err != nil {
		log.Fatalf("reading ssl certificates failed: %s", err)
	}
	return []tls.Certificate{cert}
}

func getTSLConfig(cert string) *tls.Config {

	pool := x509.NewCertPool()
	ca, err := ioutil.ReadFile(cert)
	if err != nil {
		log.Fatalf("reading certificate file failed: %s", err)
	}
	pool.AppendCertsFromPEM(ca)

	tslConfig := &tls.Config{
		RootCAs:            pool,
		InsecureSkipVerify: true,
	}

	return tslConfig
}

func ConnectToMongo() (a *mongo.Database, b *mongo.Client, err error) {
	clientOptions := options.Client()
	clientOptions.ApplyURI(os.Getenv("MONGO_URI"))
	if len(os.Getenv("CA_FILE")) > 0 {
		clientOptions.SetTLSConfig(getTSLConfig(os.Getenv("CA_FILE")))
	}
	client, err := mongo.NewClient(clientOptions)
	if err != nil {
		return &mongo.Database{}, &mongo.Client{}, err
	}
	err = client.Connect(context.TODO())
	var DB = client.Database(os.Getenv("MONGO_DB"))
	if err != nil {
		return &mongo.Database{}, &mongo.Client{}, err
	}
	err = client.Ping(context.TODO(), nil)
	if err != nil {
		return &mongo.Database{}, &mongo.Client{}, err
	}
	return DB, client, nil
}
