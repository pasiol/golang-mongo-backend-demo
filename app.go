package main

import (
	"crypto/tls"
	"encoding/json"
	"io"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/mux"
	"github.com/rs/cors"
	"go.mongodb.org/mongo-driver/mongo"
)

type App struct {
	Router    *mux.Router
	DB        *mongo.Database
	Client    *mongo.Client
	TLSConfig *tls.Config
}

func (a *App) Initialize() {

	var err error
	a.DB, a.Client, err = ConnectToMongo()
	if err != nil {
		log.Fatalf("database connection error: %s", err)
	}
	log.Printf("connected to db: %s", a.DB.Name())

	a.Router = mux.NewRouter()
	a.initializeRoutes()
}

func (a *App) initializeRoutes() {
	a.Router.HandleFunc("/api/user", a.createUser).Methods("POST")
	a.Router.HandleFunc("/api/users", a.getUsers).Methods("GET")
	a.Router.HandleFunc("/api/user/{Username:[a-zA-Z0-9 ]+}", a.deleteUser).Methods("DELETE")
}

func respondWithError(w http.ResponseWriter, code int, message string) {
	respondWithJSON(w, code, map[string]string{"error": message})
}

func respondWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	bytes, err := w.Write(response)
	if err != nil {
		log.Printf("writing response failed: %s", err)
	}
	log.Printf("response bytes %d", bytes)
}

func (a *App) createUser(w http.ResponseWriter, r *http.Request) {
	var u user
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&u); err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid request payload")
		return
	}
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			log.Printf("closing body failed: %s", err)
		}
	}(r.Body)

	if err := u.upsertUser(a.DB); err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondWithJSON(w, http.StatusCreated, u)

}

func (a *App) getUsers(w http.ResponseWriter, _ *http.Request) {
	users, err := getUsers(a.DB)
	if err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondWithJSON(w, http.StatusOK, users)
}

func (a App) deleteUser(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	log.Printf("req: %v", r.Body)
	username := vars["Username"]
	u := user{Username: username}
	if err := u.deleteUser(a.DB); err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
	}
	respondWithJSON(w, http.StatusOK, map[string]string{"result": "success"})
}

func (a *App) Run() {
	corsOptions := cors.New(cors.Options{
		AllowedOrigins:   []string{os.Getenv("ALLOWED_ORIGINS")},
		AllowCredentials: true,
		AllowedMethods:   []string{http.MethodGet, http.MethodPost, http.MethodOptions, http.MethodDelete},
		Debug:            true,
	})

	server := &http.Server{
		Addr:    ":" + os.Getenv("PORT"),
		Handler: corsOptions.Handler(a.Router),
		TLSConfig: &tls.Config{
			Certificates: readSSLCertificates(),
		},
	}

	log.Print("starting REST-api server")
	log.Fatal(server.ListenAndServeTLS("", ""))
}
